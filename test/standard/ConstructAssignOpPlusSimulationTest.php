<?php

use \phlint\Test as PhlintTest;

class ConstructAssignOpPlusSimulationTest {

  /**
   * Test undefined variable and `int` operands.
   *
   * @test @internal
   */
  static function undefinedVariableAndIntOperands () {
    PhlintTest::assertIssues('
      $foo += 1;
      $foo->baz();
    ', [
      '
        Name: $foo->baz() on line 2
        Expression `$foo->baz()` calls function `int::baz`.
        Function `int::baz` not found.
      ',
    ]);
  }

  /**
   * Test undefined variable and `array` operands.
   *
   * @test @internal
   */
  static function undefinedVariableAndArrayOperands () {
    PhlintTest::assertNoIssues('
      $foo += [];
      $foo->baz();
    ');
  }

  /**
   * Test undefined variable and `object` operands.
   *
   * @test @internal
   */
  static function undefinedVariableAndObjectOperands () {
    PhlintTest::assertIssues('
      $foo += (object) [];
      $foo->baz();
    ', [
      '
        Name: $foo->baz() on line 2
        Expression `$foo->baz()` calls function `int::baz`.
        Function `int::baz` not found.
      ',
    ]);
  }

  /**
   * Test `array` and `array` operands.
   *
   * @test @internal
   */
  static function arrayAndArrayOperands () {
    PhlintTest::assertIssues('
      function foo (array $bar) {
        $bar += [];
        $bar->baz();
      }
    ', [
      '
        Name: $bar->baz() on line 3
        Expression `$bar->baz()` calls function `array::baz`.
        Function `array::baz` not found.
      ',
    ]);
  }

  /**
   * Test `array` and `int` operands.
   *
   * @test @internal
   */
  static function arrayAndIntOperands () {
    PhlintTest::assertNoIssues('
      function foo (array $bar) {
        $bar += 1;
        $bar->baz();
      }
    ');
  }

  /**
   * Test `object` and `int` operands.
   *
   * @test @internal
   */
  static function objectAndIntOperands () {
    PhlintTest::assertIssues('
      function foo (object $bar) {
        $bar += 1;
        $bar->baz();
      }
    ', [
      '
        Name: $bar->baz() on line 3
        Expression `$bar->baz()` calls function `int::baz`.
        Function `int::baz` not found.
      ',
    ]);
  }

  /**
   * Test `if` condition simulation.
   *
   * @test @internal
   */
  static function ifCondition () {
    PhlintTest::assertIssues('
      function foo ($bar) {
        if (($bar += 2) + 1)
          $bar->baz();
      }
    ', [
      '
        Name: $bar->baz() on line 3
        Expression `$bar->baz()` calls function `float::baz`.
        Function `float::baz` not found.
      ',
      '
        Name: $bar->baz() on line 3
        Expression `$bar->baz()` calls function `int::baz`.
        Function `int::baz` not found.
      ',
    ]);
  }

  /**
   * Test array merge.
   *
   * @test @internal
   */
  static function arrayMerge () {
    PhlintTest::assertNoIssues('
      $foo = [];
      $foo += array_fill(0, 1, "bar");
      #$foo += array_merge($foo, array_fill(0, 1, "baz"));
      $foo -= 1;
    ');
  }

}
