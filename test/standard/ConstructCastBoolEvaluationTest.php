<?php

use \phlint\Test as PhlintTest;

class ConstructCastBoolEvaluationTest {

  /**
   * Test in combination with various types.
   *
   * @test @internal
   */
  static function variousTypesTest () {
    PhlintTest::assertIssues('
      dump((bool) "a");
      dump((bool) (bool) 1);
      dump((bool) (bool) null);
      dump((bool) 1);
      dump((bool) false);
      dump((bool) null);
    ', [
      '
        Dump: dump((bool) "a") on line 1
        Argument `(bool) "a"` evaluates to `bool(true)`.
      ',
      '
        Dump: dump((bool) (bool) 1) on line 2
        Argument `(bool) (bool) 1` evaluates to `bool(true)`.
      ',
      '
        Dump: dump((bool) (bool) null) on line 3
        Argument `(bool) (bool) null` evaluates to `bool(false)`.
      ',
      '
        Dump: dump((bool) 1) on line 4
        Argument `(bool) 1` evaluates to `bool(true)`.
      ',
      '
        Dump: dump((bool) false) on line 5
        Argument `(bool) false` evaluates to `bool(false)`.
      ',
      '
        Dump: dump((bool) null) on line 6
        Argument `(bool) null` evaluates to `bool(false)`.
      ',
    ]);
  }

}
