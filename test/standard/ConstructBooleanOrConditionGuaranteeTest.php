<?php

use \phlint\Test as PhlintTest;

class ConstructBooleanOrConditionGuaranteeTest {

  /**
   * Test that disjoint guarantees do not cause any unexpected issues.
   *
   * @test @internal
   */
  static function disjointGuaranteesNoIssues () {
    PhlintTest::assertNoIssues('
      function foo ($bar, $baz) {
        if (is_null($bar) || is_null($baz))
          return [$bar, $baz];
      }
    ');
  }

}
