<?php

use \phlint\Test as PhlintTest;

class ConstructCastArrayEvaluationTest {

  /**
   * Test in combination with various types.
   *
   * @test @internal
   */
  static function variousTypesTest () {
    PhlintTest::assertIssues('
      dump((array) "a");
      dump((array) (array) 1);
      dump((array) (array) null);
      dump((array) 1);
      dump((array) false);
      dump((array) null);
    ', [
      '
        Dump: dump((array) "a") on line 1
        Argument `(array) "a"` evaluates to `[string("a")]`.
      ',
      '
        Dump: dump((array) (array) 1) on line 2
        Argument `(array) (array) 1` evaluates to `[int(1)]`.
      ',
      '
        Dump: dump((array) (array) null) on line 3
        Argument `(array) (array) null` evaluates to `[]`.
      ',
      '
        Dump: dump((array) 1) on line 4
        Argument `(array) 1` evaluates to `[int(1)]`.
      ',
      '
        Dump: dump((array) false) on line 5
        Argument `(array) false` evaluates to `[bool(false)]`.
      ',
      '
        Dump: dump((array) null) on line 6
        Argument `(array) null` evaluates to `[]`.
      ',
    ]);
  }

}
