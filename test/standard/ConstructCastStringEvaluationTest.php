<?php

use \phlint\Test as PhlintTest;

class ConstructCastStringEvaluationTest {

  /**
   * Test in combination with various types.
   *
   * @test @internal
   */
  static function variousTypesTest () {
    PhlintTest::assertIssues('
      dump((string) "a");
      dump((string) (string) 1);
      dump((string) (string) null);
      dump((string) 1);
      dump((string) false);
      dump((string) null);
    ', [
      '
        Dump: dump((string) "a") on line 1
        Argument `(string) "a"` evaluates to `string("a")`.
      ',
      '
        Dump: dump((string) (string) 1) on line 2
        Argument `(string) (string) 1` evaluates to `string(\'1\')`.
      ',
      '
        Dump: dump((string) (string) null) on line 3
        Argument `(string) (string) null` evaluates to `string(\'\')`.
      ',
      '
        Dump: dump((string) 1) on line 4
        Argument `(string) 1` evaluates to `string(\'1\')`.
      ',
      '
        Dump: dump((string) false) on line 5
        Argument `(string) false` evaluates to `string(\'\')`.
      ',
      '
        Dump: dump((string) null) on line 6
        Argument `(string) null` evaluates to `string(\'\')`.
      ',
    ]);
  }

}
