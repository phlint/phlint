<?php

use \phlint\Test as PhlintTest;

class ConstructMethodCallNameLinkTest {

  /**
   * Test that method call linking is done correctly in
   * case of mixed object type.
   *
   * @test @internal
   */
  static function mixedObjectTypeLink () {
    PhlintTest::assertIssues('
      function foo ($a) {
        if ($a instanceof B) {
          bar($a);
        } elseif ($a instanceof C) {}
        bar($a);
      }
      function bar ($a) {
        $a->foo();
      }
    ', [
      '
        Name: $a->foo() on line 8
        Expression `$a->foo()` calls function `B::foo`.
        Function `B::foo` not found.
          Trace #1:
            #1: Function *function bar (B $a)* specialized for the expression *bar($a)* on line 3.
      ',
    ]);
  }

}
