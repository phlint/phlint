<?php

use \phlint\Test as PhlintTest;

class ConstructForeachSimulationTest {

  /**
   * Test key and value variable initialization.
   *
   * @test @internal
   */
  static function variableInitialization () {
    PhlintTest::assertNoIssues('
      function foo ($bar) {
        foreach ($bar as $key => $value)
          return [$key, $value];
      }
    ');
  }

}
