<?php

use \phlint\Test as PhlintTest;

class ConstructAssignSimulationTest {

  /**
   * Test assignment simulation inside an argument.
   *
   * @test @internal
   */
  static function funcCallArg () {
    PhlintTest::assertIssues('
      function foo ($bar) {
        if (uniqid("", ($baz = is_int($bar))))
          $baz->fun();
      }
    ', [
      '
        Name: $baz->fun() on line 3
        Expression `$baz->fun()` calls function `bool::fun`.
        Function `bool::fun` not found.
      ',
    ]);
  }

}
