<?php

use \phlint\Test as PhlintTest;

class ConstructAssignOpConcatSimulationTest {

  /**
   * Test `if` condition simulation.
   *
   * @test @internal
   */
  static function ifCondition () {
    PhlintTest::assertIssues('
      function foo ($bar) {
        if (($bar .= "!") . "?")
          $bar->baz();
      }
    ', [
      '
        Name: $bar->baz() on line 3
        Expression `$bar->baz()` calls function `string::baz`.
        Function `string::baz` not found.
      ',
    ]);
  }

}
