<?php

use \phlint\Test as PhlintTest;

class TemplateSpecializationInfiniteRecursionTest {

  /**
   * Test that dynamically limited recursion does not cause a infinitely
   * recursive specialization.
   *
   * @test @internal
   */
  static function dynamicallyLimitedRecursion () {
    PhlintTest::assertNoIssues('
      function foo ($bar, $baz = 0) {
        if (is_array($bar))
          foreach ($bar as $fun)
            return callFoo($fun, $baz + 1);
        return $bar;
      }
      function callFoo ($bar, $baz) {
        return foo($bar, $baz);
      }
    ');
  }

}
