<?php

use \phlint\Test as PhlintTest;

class ConstructBinaryOpLogicalOrSimulationTest {

  /**
   * Test in combination with various types.
   *
   * @test @internal
   */
  static function variousTypesTest () {
    PhlintTest::assertIssues('
      dump(1 or 1);
      dump("a" or 2);
      dump(null or false);
    ', [
      '
        Dump: dump(1 or 1) on line 1
        Argument `1 or 1` evaluates to `bool(true)`.
      ',
      '
        Dump: dump("a" or 2) on line 2
        Argument `"a" or 2` evaluates to `bool(true)`.
      ',
      '
        Dump: dump(null or false) on line 3
        Argument `null or false` evaluates to `bool(false)`.
      ',
    ]);
  }

}
