<?php

use \phlint\Test as PhlintTest;

class ConstructCastIntEvaluationTest {

  /**
   * Test in combination with various types.
   *
   * @test @internal
   */
  static function variousTypesTest () {
    PhlintTest::assertIssues('
      dump((int) "a");
      dump((int) (int) 1);
      dump((int) (int) null);
      dump((int) 1);
      dump((int) false);
      dump((int) null);
    ', [
      '
        Dump: dump((int) "a") on line 1
        Argument `(int) "a"` evaluates to `int(0)`.
      ',
      '
        Dump: dump((int) (int) 1) on line 2
        Argument `(int) (int) 1` evaluates to `int(1)`.
      ',
      '
        Dump: dump((int) (int) null) on line 3
        Argument `(int) (int) null` evaluates to `int(0)`.
      ',
      '
        Dump: dump((int) 1) on line 4
        Argument `(int) 1` evaluates to `int(1)`.
      ',
      '
        Dump: dump((int) false) on line 5
        Argument `(int) false` evaluates to `int(0)`.
      ',
      '
        Dump: dump((int) null) on line 6
        Argument `(int) null` evaluates to `int(0)`.
      ',
    ]);
  }

}
