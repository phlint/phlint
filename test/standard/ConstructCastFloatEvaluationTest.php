<?php

use \phlint\Test as PhlintTest;

class ConstructCastFloatEvaluationTest {

  /**
   * Test in combination with various types.
   *
   * @test @internal
   */
  static function variousTypesTest () {
    PhlintTest::assertIssues('
      dump((float) "a");
      dump((float) (float) 1);
      dump((float) (float) null);
      dump((float) 1);
      dump((float) false);
      dump((float) null);
    ', [
      '
        Dump: dump((double) "a") on line 1
        Argument `(double) "a"` evaluates to `float(0.0)`.
      ',
      '
        Dump: dump((double) (double) 1) on line 2
        Argument `(double) (double) 1` evaluates to `float(1.0)`.
      ',
      '
        Dump: dump((double) (double) null) on line 3
        Argument `(double) (double) null` evaluates to `float(0.0)`.
      ',
      '
        Dump: dump((double) 1) on line 4
        Argument `(double) 1` evaluates to `float(1.0)`.
      ',
      '
        Dump: dump((double) false) on line 5
        Argument `(double) false` evaluates to `float(0.0)`.
      ',
      '
        Dump: dump((double) null) on line 6
        Argument `(double) null` evaluates to `float(0.0)`.
      ',
    ]);
  }

}
