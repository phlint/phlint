<?php

use \phlint\Test as PhlintTest;

class ConstructAssignOpPowSimulationTest {

  /**
   * Test `if` condition simulation.
   *
   * @test @internal
   */
  static function ifCondition () {
    PhlintTest::assertIssues('
      function foo ($bar) {
        if (($bar **= 2) + 1)
          $bar->baz();
      }
    ', [
      '
        Name: $bar->baz() on line 3
        Expression `$bar->baz()` calls function `float::baz`.
        Function `float::baz` not found.
      ',
      '
        Name: $bar->baz() on line 3
        Expression `$bar->baz()` calls function `int::baz`.
        Function `int::baz` not found.
      ',
    ]);
  }

}
