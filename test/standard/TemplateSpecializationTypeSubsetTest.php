<?php

use \phlint\Test as PhlintTest;

class TemplateSpecializationTypeSubsetTest {

  /**
   * Test that a specialization does not get a type which is a not a subset
   * of the constraints.
   *
   * @test @internal
   */
  static function specializeWithSupersetArugments () {
    PhlintTest::assertIssues('
      function foo ($bar, array $baz) {
        return 1 + $bar;
      }
      foo("", array_reduce());
    ', [
      '
        Operand Compatibility: $bar on line 2
        Variable `$bar` is always or sometimes of type `string`.
        Expression `1 + $bar` may cause undesired or unexpected behavior with `string` operands.
          Trace #1:
            #1: Function *function foo ("" $bar, array $baz)*
              specialized for the expression *foo("", array_reduce())* on line 4.
      ',
    ]);
  }

}
