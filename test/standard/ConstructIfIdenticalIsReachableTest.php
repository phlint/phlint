<?php

use \phlint\Test as PhlintTest;

class ConstructIfIdenticalIsReachableTest {

  /**
   * Test that simulation does not go past `if` if it is not reachable.
   *
   * @test @internal
   */
  static function isNullLeftSide () {
    PhlintTest::assertNoIssues('
      /** @param null $bar */
      function foo ($bar) {
        if (null === $bar)
          return;
        $bar->baz();
      }
    ');
  }
  /**
   * Test that simulation does not go past `if` if it is not reachable.
   *
   * @test @internal
   */
  static function isNullRightSide () {
    PhlintTest::assertNoIssues('
      /** @param null $bar */
      function foo ($bar) {
        if ($bar === null)
          return;
        $bar->baz();
      }
    ');
  }

}
