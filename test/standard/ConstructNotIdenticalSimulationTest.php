<?php

use \phlint\Test as PhlintTest;

class ConstructNotIdenticalSimulationTest {

  /**
   * Test yield propagation from an assignment in condition.
   *
   * @test @internal
   */
  static function rightSideAssignConditionYieldPropagation () {
    PhlintTest::assertIssues('
      function foo($bar, $baz = null) {
        if ($bar !== ($fun = ZEND_DEBUG_BUILD))
          return $fun;
        return $baz;
      }
      foo($_GET["a"], []) . "b";
    ', [
      '
        Operand Compatibility: foo($_GET["a"], []) on line 6
        Expression `foo($_GET["a"], [])` is always or sometimes of type `bool`.
        Expression `foo($_GET["a"], []) . "b"`
          may cause undesired or unexpected behavior with `bool` operands.
      ',
      '
        Operand Compatibility: foo($_GET["a"], []) on line 6
        Expression `foo($_GET["a"], [])` is always or sometimes of type `mixed[int|string]`.
        Expression `foo($_GET["a"], []) . "b"`
          may cause undesired or unexpected behavior with `mixed[int|string]` operands.
      ',
    ]);
  }

}
