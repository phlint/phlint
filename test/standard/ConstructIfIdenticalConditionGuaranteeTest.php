<?php

use \phlint\Test as PhlintTest;

class ConstructIfIdenticalConditionGuaranteeTest {

  /**
   * Test condition guarantee filtering.
   *
   * @test @internal
   */
  static function isNullLeftSide () {
    PhlintTest::assertIssues('
      /** @param null $bar */
      function foo ($bar) {
        $bar = ZEND_DEBUG_BUILD ? $bar : false;
        if (null === $bar)
          return;
        $bar->baz();
      }
    ', [
      '
        Name: $bar->baz() on line 6
        Expression `$bar->baz()` calls function `bool::baz`.
        Function `bool::baz` not found.
      ',
    ]);
  }
  /**
   * Test condition guarantee filtering.
   *
   * @test @internal
   */
  static function isNullRightSide () {
    PhlintTest::assertIssues('
      /** @param null $bar */
      function foo ($bar) {
        $bar = ZEND_DEBUG_BUILD ? $bar : false;
        if ($bar === null)
          return;
        $bar->baz();
      }
    ', [
      '
        Name: $bar->baz() on line 6
        Expression `$bar->baz()` calls function `bool::baz`.
        Function `bool::baz` not found.
      ',
    ]);
  }

}
