<?php

use \phlint\Test as PhlintTest;

class ConstructIfAssignConditionGuaranteeTest {

  /**
   * Test assignment guarantee.
   *
   * @test @internal
   */
  static function ifAssign () {
    PhlintTest::assertIssues('
      function foo () {
        if (rand(0, 1))
          return null;
        else if (rand(0, 1))
          return true;
        else if (rand(0, 1))
          return false;
        else if (rand(0, 1))
          return 0;
        else if (rand(0, 1))
          return -1;
        else if (rand(0, 1))
          return 1;
        else if (rand(0, 1))
          return 0.0;
        else if (rand(0, 1))
          return 0.1;
        else if (rand(0, 1))
          return -0.1;
        else if (rand(0, 1))
          return "";
        else if (rand(0, 1))
          return "abc";
        else if (rand(0, 1))
          return [];
        else if (rand(0, 1))
          return (object) [];
        else if (rand(0, 1))
          return new stdClass();
        else if (rand(0, 1))
          return function () {};
      }
      if ($bar = foo())
        $bar->baz();
    ', [
      '
        Name: $bar->baz() on line 34
        Expression `$bar->baz()` calls function `bool::baz`.
        Function `bool::baz` not found.
      ',
      '
        Name: $bar->baz() on line 34
        Expression `$bar->baz()` calls function `int::baz`.
        Function `int::baz` not found.
      ',
      '
        Name: $bar->baz() on line 34
        Expression `$bar->baz()` calls function `float::baz`.
        Function `float::baz` not found.
      ',
      '
        Name: $bar->baz() on line 34
        Expression `$bar->baz()` calls function `string::baz`.
        Function `string::baz` not found.
      ',
      '
        Name: $bar->baz() on line 34
        Expression `$bar->baz()` calls function `stdClass::baz`.
        Function `stdClass::baz` not found.
      ',
    ]);
  }

  /**
   * Test negative assignment guarantee.
   *
   * @test @internal
   */
  static function ifAssignNegative () {
    PhlintTest::assertIssues('
      function foo () {
        if (rand(0, 1))
          return null;
        else if (rand(0, 1))
          return true;
        else if (rand(0, 1))
          return false;
        else if (rand(0, 1))
          return 0;
        else if (rand(0, 1))
          return -1;
        else if (rand(0, 1))
          return 1;
        else if (rand(0, 1))
          return 0.0;
        else if (rand(0, 1))
          return 0.1;
        else if (rand(0, 1))
          return -0.1;
        else if (rand(0, 1))
          return "";
        else if (rand(0, 1))
          return "abc";
        else if (rand(0, 1))
          return [];
        else if (rand(0, 1))
          return (object) [];
        else if (rand(0, 1))
          return new stdClass();
        else if (rand(0, 1))
          return function () {};
      }
      if (!($bar = foo()))
        $bar->baz();
    ', [
      '
        Name: $bar->baz() on line 34
        Expression `$bar->baz()` calls function `null::baz`.
        Function `null::baz` not found.
      ',
      '
        Name: $bar->baz() on line 34
        Expression `$bar->baz()` calls function `bool::baz`.
        Function `bool::baz` not found.
      ',
      '
        Name: $bar->baz() on line 34
        Expression `$bar->baz()` calls function `int::baz`.
        Function `int::baz` not found.
      ',
      '
        Name: $bar->baz() on line 34
        Expression `$bar->baz()` calls function `float::baz`.
        Function `float::baz` not found.
      ',
      '
        Name: $bar->baz() on line 34
        Expression `$bar->baz()` calls function `string::baz`.
        Function `string::baz` not found.
      ',
      '
        Name: $bar->baz() on line 34
        Expression `$bar->baz()` calls function `mixed[int|string]::baz`.
        Function `mixed[int|string]::baz` not found.
      ',
    ]);
  }

}
