<?php

use \phlint\Test as PhlintTest;

class FeatureLateStaticBindingsTemplateSpecializationTest {

  /**
   * Test retuning `$this` with return type and no template specialization.
   *
   * @test @internal
   */
  static function returnThisWithNoTemplate () {
    PhlintTest::assertIssues('
      class A extends B {}
      class B {
        /**
         * @return B
         */
        function foo () {
          return $this;
        }
      }
      (new A())->foo()->bar();
    ', [
      '
        Name: (new A())->foo()->bar() on line 10
        Expression `(new A())->foo()->bar()` calls function `B::bar`.
        Function `B::bar` not found.
      ',
    ]);
  }

  /**
   * Test retuning `$this` with return type and template specialization.
   *
   * @test @internal
   */
  static function returnThisWithTemplate () {
    PhlintTest::assertIssues('
      class A extends B {}
      class B {
        /**
         * @return B
         */
        function foo ($bar) {
          return $this;
        }
      }
      (new A())->foo("bar")->baz();
    ', [
      '
        Name: (new A())->foo("bar")->baz() on line 10
        Expression `(new A())->foo("bar")->baz()` calls function `A::baz`.
        Function `A::baz` not found.
      ',
      '
        Name: (new A())->foo("bar")->baz() on line 10
        Expression `(new A())->foo("bar")->baz()` calls function `B::baz`.
        Function `B::baz` not found.
      ',
    ]);
  }

  /**
   * Test that context yield specialization happens only with real context type.
   *
   * @test @internal
   */
  static function unrelatedContextYieldType () {
    PhlintTest::assertIssues('
      class A {
        /** @return B */
        static function foo () {
          return new A();
        }
        function bar () {
          return $this;
        }
      }
      class B {}
      A::foo()->bar()->baz();
    ', [
      '
        Name: A::foo()->bar() on line 11
        Expression `A::foo()->bar()` calls function `B::bar`.
        Function `B::bar` not found.
      ',
      '
        Name: A::foo()->bar()->baz() on line 11
        Expression `A::foo()->bar()->baz()` calls function `A::baz`.
        Function `A::baz` not found.
      ',
    ]);
  }

  /**
   * Test that context yield specialization happens only with real
   * defined or extended context type.
   *
   * @test @internal
   */
  static function unrelatedContextYieldTypeExtended () {
    PhlintTest::assertIssues('
      class A {
        function foo () {
          return $this;
        }
        function bar () {
          dump($this);
        }
      }
      class B extends A {}
      class C {
        function foo () {
          return $this;
        }
        function bar () {
          dump($this);
        }
      }
      class D extends C {}
      $baz = (rand(0, 1) ? new A() : (rand(0, 1) ? new B() : (rand(0, 1) ? new C() : new D())));
      $baz->foo()->bar();
    ', [
      '
        Dump: dump($this) on line 6
        Argument `$this` evaluates to `A`.
      ',
      '
        Dump: dump($this) on line 15
        Argument `$this` evaluates to `C`.
      ',
      '
        Dump: dump($this) on line 6
        Argument `$this` evaluates to `A|B`.
          Trace #1:
            #1: Method *function bar()* specialized for the expression *$baz->foo()->bar()* on line 20.
      ',
      '
        Dump: dump($this) on line 15
        Argument `$this` evaluates to `C|D`.
          Trace #1:
            #1: Method *function bar()* specialized for the expression *$baz->foo()->bar()* on line 20.
      ',
    ]);
  }

}
