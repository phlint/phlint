<?php

use \phlint\Test as PhlintTest;

class ConstructBooleanOrSimulationTest {

  /**
   * Test simulation of condition with assignment.
   *
   * @test @internal
   */
  static function conditionalAssignmentSimulation () {
    PhlintTest::assertIssues('
      ($foo = (ZEND_DEBUG_BUILD ? true : false)) || $foo = "a";
      $foo + 1;
    ', [
      '
        Operand Compatibility: $foo on line 2
        Variable `$foo` is always or sometimes of type `string`.
        Expression `$foo + 1` may cause undesired or unexpected behavior with `string` operands.
      ',
      '
        Operand Compatibility: $foo on line 2
        Variable `$foo` is always or sometimes of type `bool`.
        Expression `$foo + 1` may cause undesired or unexpected behavior with `bool` operands.
      ',
    ]);
  }

  /**
   * Test simulation of condition with assignment on an existing variable.
   *
   * @test @internal
   */
  static function conditionalAssignmentSimulationExistingVariable () {
    PhlintTest::assertIssues('
      $foo = null;
      ($foo = (ZEND_DEBUG_BUILD ? true : false)) || $foo = "a";
      $foo + 1;
    ', [
      '
        Operand Compatibility: $foo on line 3
        Variable `$foo` is always or sometimes of type `string`.
        Expression `$foo + 1` may cause undesired or unexpected behavior with `string` operands.
      ',
      '
        Operand Compatibility: $foo on line 3
        Variable `$foo` is always or sometimes of type `bool`.
        Expression `$foo + 1` may cause undesired or unexpected behavior with `bool` operands.
      ',
    ]);
  }

}
