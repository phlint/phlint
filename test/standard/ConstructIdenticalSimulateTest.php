<?php

use \phlint\Test as PhlintTest;

class ConstructIdenticalSimulateTest {

  /**
   * Test left side propagation.
   *
   * @test @internal
   */
  static function leftSidePropagation () {
    PhlintTest::assertIssues('
      ($foo = ZEND_DEBUG_BUILD) === null;
      $foo->bar();
    ', [
      '
        Name: $foo->bar() on line 2
        Expression `$foo->bar()` calls function `bool::bar`.
        Function `bool::bar` not found.
      ',
    ]);
  }

  /**
   * Test right side propagation.
   *
   * @test @internal
   */
  static function rightSidePropagation () {
    PhlintTest::assertIssues('
      null === ($foo = ZEND_DEBUG_BUILD);
      $foo->bar();
    ', [
      '
        Name: $foo->bar() on line 2
        Expression `$foo->bar()` calls function `bool::bar`.
        Function `bool::bar` not found.
      ',
    ]);
  }

}
