<?php

use \phlint\Test as PhlintTest;

class ConstructAssignVariableThisSimulateTest {

  /**
   * Test simulation of `$this`.
   *
   * @test @internal
   */
  static function thisInstance () {
    PhlintTest::assertIssues('
      class A {
        function foo () {
          $bar = $this;
          $bar->baz();
        }
      }
    ', [
      '
        Name: $bar->baz() on line 4
        Expression `$bar->baz()` calls function `A::baz`.
        Function `A::baz` not found.
      ',
    ]);
  }

}
