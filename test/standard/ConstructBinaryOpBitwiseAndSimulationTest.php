<?php

use \phlint\Test as PhlintTest;

class ConstructBinaryOpBitwiseAndSimulationTest {

  /**
   * Test in combination with various types.
   *
   * @test @internal
   */
  static function variousTypesTest () {
    PhlintTest::assertIssues('
      dump(1 & 1);
      dump("a" & 2);
      dump(null & false);
    ', [
      '
        Dump: dump(1 & 1) on line 1
        Argument `1 & 1` evaluates to `int(1)`.
      ',
      '
        Dump: dump("a" & 2) on line 2
        Argument `"a" & 2` evaluates to `int(0)`.
      ',
      '
        Dump: dump(null & false) on line 3
        Argument `null & false` evaluates to `int(0)`.
      ',
    ]);
  }

}
