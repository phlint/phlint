<?php

use \phlint\Test as PhlintTest;

class TemplateSpecializationMultiTypeTest {

  /**
   * Test that a template with a `mixed` and `bool` parameter gets specialized.
   *
   * @test @internal
   */
  static function templateWithMixedAndBoolParameter () {
    PhlintTest::assertIssues('
      /**
       * @param mixed|bool $bar
       * @return string
       */
      function foo ($bar) {
        return $bar . "!";
      }
      foo(null);
    ', [
      '
        Operand Compatibility: $bar on line 6
        Variable `$bar` is always or sometimes of type `bool`.
        Expression `$bar . "!"` may cause undesired or unexpected behavior with `bool` operands.
      ',
      '
        Operand Compatibility: $bar on line 6
        Variable `$bar` is always or sometimes of type `null`.
        Expression `$bar . "!"` may cause undesired or unexpected behavior with `null` operands.
          Trace #1:
            #1: Function *function foo (null $bar)* specialized for the expression *foo(null)* on line 8.
      ',
    ]);
  }

  /**
   * Test that a function with strict types does not get specialized.
   *
   * @test @internal
   */
  static function templateWithStrictTypes () {
    PhlintTest::assertIssues('
      function foo (bool $bar) : string {
        return $bar . "!";
      }
      foo(null);
    ', [
      '
        Operand Compatibility: $bar on line 2
        Variable `$bar` is always or sometimes of type `bool`.
        Expression `$bar . "!"` may cause undesired or unexpected behavior with `bool` operands.
      ',
      '
        Argument Compatibility: null on line 4
        Argument #1 passed in the expression `foo(null)` is of type `null`.
        A value of type `null` is not implicitly convertible to type `bool`.
      ',
    ]);
  }

  /**
   * Test that a default null values does not impact specialization values.
   *
   * @test @internal
   */
  static function templateWithDefaultNullParameter () {
    PhlintTest::assertIssues('
      /**
       * @param mixed $bar
       * @return mixed
       */
      function foo ($bar = null) {
        return $bar;
      }
      foo(0)->baz();
    ', [
      '
        Name: foo(0)->baz() on line 8
        Expression `foo(0)->baz()` calls function `int::baz`.
        Function `int::baz` not found.
      ',
    ]);
  }

}
