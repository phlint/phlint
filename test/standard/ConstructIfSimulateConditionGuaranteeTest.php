<?php

use \phlint\Test as PhlintTest;

class ConstructIfSimulateConditionGuaranteeTest {

  /**
   * Test that one guarantee simulated after a certain scope does not
   * propagate past another conflicting guarantee.
   *
   * @test @internal
   */
  static function overwriteConflictingGuarantee () {
    PhlintTest::assertNoIssues('
      function foo ($bar) {
        if (!is_int($bar))
          return;
        if (!isset($bar))
          $bar->baz();
        return $bar;
      }
    ');
  }

  /**
   * Test that one guarantee simulated after a certain scope does not
   * propagate past another narrowing guarantee.
   *
   * @test @internal
   */
  static function narrowConflictingGuarantee () {
    PhlintTest::assertIssues('
      function foo ($bar) {
        if (!isset($bar))
          return;
        if (!is_int($bar))
          return;
        $bar->baz();
      }
    ', [
      '
        Name: $bar->baz() on line 6
        Expression `$bar->baz()` calls function `int::baz`.
        Function `int::baz` not found.
      ',
    ]);
  }

  /**
   * Test that one empty guarantee simulated after a certain scope does not
   * propagate past another conflicting guarantee.
   *
   * @test @internal
   */
  static function overwriteConflictingGuaranteeEmpty () {
    PhlintTest::assertNoIssues('
      function foo ($bar) {
        if (!empty($bar) && !is_numeric($bar))
          return $bar . "!";
        if (!empty($bar) && $bar < 0)
          return $bar . "!";
        return $bar;
      }
    ');
  }

  /**
   * Test a multi type variable simulation through a multi type
   * narrowing condition guarantee.
   *
   * @test @internal
   */
  static function multiTypeConditionGuaranteeNarrowing () {
    PhlintTest::assertIssues('

      class A {}
      class B {}
      class C {}
      class D {}
      class E {}
      class F {}
      class G {}
      class H {}

      /**
       * @param A|B|C|D|E|F $bar
       */
      function foo ($bar) {
        if ($bar instanceof B || $bar instanceof C || $bar instanceof D || $bar instanceof E || $bar instanceof G) {
          $bar->baz();
          if ($bar instanceof C || $bar instanceof D || $bar instanceof H)
            $bar->baz();
          else
            $bar->baz();
          $bar->baz();
        }
      }
    ', [
      '
        Name: $bar->baz() on line 16
        Expression `$bar->baz()` calls function `B::baz`.
        Function `B::baz` not found.
      ',
      '
        Name: $bar->baz() on line 16
        Expression `$bar->baz()` calls function `C::baz`.
        Function `C::baz` not found.
      ',
      '
        Name: $bar->baz() on line 16
        Expression `$bar->baz()` calls function `D::baz`.
        Function `D::baz` not found.
      ',
      '
        Name: $bar->baz() on line 16
        Expression `$bar->baz()` calls function `E::baz`.
        Function `E::baz` not found.
      ',
      '
        Name: $bar->baz() on line 18
        Expression `$bar->baz()` calls function `C::baz`.
        Function `C::baz` not found.
      ',
      '
        Name: $bar->baz() on line 18
        Expression `$bar->baz()` calls function `D::baz`.
        Function `D::baz` not found.
      ',
      '
        Name: $bar->baz() on line 20
        Expression `$bar->baz()` calls function `B::baz`.
        Function `B::baz` not found.
      ',
      '
        Name: $bar->baz() on line 20
        Expression `$bar->baz()` calls function `E::baz`.
        Function `E::baz` not found.
      ',
      '
        Name: $bar->baz() on line 21
        Expression `$bar->baz()` calls function `B::baz`.
        Function `B::baz` not found.
      ',
      '
        Name: $bar->baz() on line 21
        Expression `$bar->baz()` calls function `C::baz`.
        Function `C::baz` not found.
      ',
      '
        Name: $bar->baz() on line 21
        Expression `$bar->baz()` calls function `D::baz`.
        Function `D::baz` not found.
      ',
      '
        Name: $bar->baz() on line 21
        Expression `$bar->baz()` calls function `E::baz`.
        Function `E::baz` not found.
      ',
    ]);
  }

  /**
   * Test a multi type variable simulation through a multi type
   * excluding condition guarantee.
   *
   * @test @internal
   */
  static function multiTypeConditionGuaranteeExcluding () {
    PhlintTest::assertIssues('

      class A {}
      class B {}
      class C {}

      /**
       * @param A|B|C $bar
       */
      function foo ($bar) {
        $bar->baz();
        if (!empty($bar) && !is_int($bar))
          $bar->baz();
        else
          $bar->baz();
        $bar->baz();
      }

    ', [
      '
        Name: $bar->baz() on line 10
        Expression `$bar->baz()` calls function `A::baz`.
        Function `A::baz` not found.
      ',
      '
        Name: $bar->baz() on line 10
        Expression `$bar->baz()` calls function `B::baz`.
        Function `B::baz` not found.
      ',
      '
        Name: $bar->baz() on line 10
        Expression `$bar->baz()` calls function `C::baz`.
        Function `C::baz` not found.
      ',
      '
        Name: $bar->baz() on line 12
        Expression `$bar->baz()` calls function `A::baz`.
        Function `A::baz` not found.
      ',
      '
        Name: $bar->baz() on line 12
        Expression `$bar->baz()` calls function `B::baz`.
        Function `B::baz` not found.
      ',
      '
        Name: $bar->baz() on line 12
        Expression `$bar->baz()` calls function `C::baz`.
        Function `C::baz` not found.
      ',
      '
        Name: $bar->baz() on line 15
        Expression `$bar->baz()` calls function `A::baz`.
        Function `A::baz` not found.
      ',
      '
        Name: $bar->baz() on line 15
        Expression `$bar->baz()` calls function `B::baz`.
        Function `B::baz` not found.
      ',
      '
        Name: $bar->baz() on line 15
        Expression `$bar->baz()` calls function `C::baz`.
        Function `C::baz` not found.
      ',
    ]);
  }

}
