<?php

use \phlint\Test as PhlintTest;

class StandardSymbolIsArrayConditionGuaranteeTest {

  /**
   * Test `is_array` in a deterministic type conversion function.
   *
   * @test @internal
   */
  static function deterministicTypeConversion () {
    PhlintTest::assertIssues('
      $foo = ZEND_DEBUG_BUILD ? ["a"] : "a";
      if (!is_array($foo))
        $foo = [$foo . "!"];
      $foo->bar();
    ', [
      '
        Name: $foo->bar() on line 4
        Expression `$foo->bar()` calls function `string[]::bar`.
        Function `string[]::bar` not found.
      ',
    ]);
  }

}
