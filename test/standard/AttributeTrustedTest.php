<?php

use \phlint\Test as PhlintTest;

class AttributeTrustedTest {

  /**
   * Test `@trusted` on non-existing method call.
   *
   * @test @internal
   */
  static function methodCall () {
    PhlintTest::assertIssues('
      class A {}

      /** @trusted */
      (new A())->foo();

      (new A())->bar();
    ', [
      '
        Name: (new A())->bar() on line 6
        Expression `(new A())->bar()` calls function `A::bar`.
        Function `A::bar` not found.
      ',
    ]);
  }

}
