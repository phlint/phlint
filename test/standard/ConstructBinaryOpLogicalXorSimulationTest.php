<?php

use \phlint\Test as PhlintTest;

class ConstructBinaryOpLogicalXorSimulationTest {

  /**
   * Test in combination with various types.
   *
   * @test @internal
   */
  static function variousTypesTest () {
    PhlintTest::assertIssues('
      dump(1 xor 1);
      dump("a" xor 2);
      dump(null xor false);
    ', [
      '
        Dump: dump(1 xor 1) on line 1
        Argument `1 xor 1` evaluates to `bool(false)`.
      ',
      '
        Dump: dump("a" xor 2) on line 2
        Argument `"a" xor 2` evaluates to `bool(false)`.
      ',
      '
        Dump: dump(null xor false) on line 3
        Argument `null xor false` evaluates to `bool(false)`.
      ',
    ]);
  }

}
