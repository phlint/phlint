<?php

use \phlint\Test as PhlintTest;

class ConstructIfNotIdenticalConditionGuaranteeTest {

  /**
   * Test condition guarantee filtering.
   *
   * @test @internal
   */
  static function isNullRightSide () {
    PhlintTest::assertIssues('
      function foo ($bar) {
        if ($bar !== null)
          {}
        else
          $bar->baz();
      }
    ', [
      '
        Name: $bar->baz() on line 5
        Expression `$bar->baz()` calls function `null::baz`.
        Function `null::baz` not found.
      ',
    ]);
  }
  /**
   * Test condition guarantee filtering.
   *
   * @test @internal
   */
  static function isNullLeftSide () {
    PhlintTest::assertIssues('
      function foo ($bar) {
        if (null !== $bar)
          {}
        else
          $bar->baz();
      }
    ', [
      '
        Name: $bar->baz() on line 5
        Expression `$bar->baz()` calls function `null::baz`.
        Function `null::baz` not found.
      ',
    ]);
  }

  /**
   * Test condition guarantee filtering.
   *
   * @test @internal
   */
  static function isNullRightSideWithDeclaration () {
    PhlintTest::assertIssues('
      /**
       * @param array|null $bar
       */
      function foo (array $bar = null) {
        if ($bar !== null)
          $bar->baz();
      }
    ', [
      '
        Name: $bar->baz() on line 6
        Expression `$bar->baz()` calls function `array::baz`.
        Function `array::baz` not found.
      ',
    ]);
  }
  /**
   * Test condition guarantee filtering.
   *
   * @test @internal
   */
  static function isNullLeftSideWithDeclaration () {
    PhlintTest::assertIssues('
      /**
       * @param array|null $bar
       */
      function foo (array $bar = null) {
        if (null !== $bar)
          $bar->baz();
      }
    ', [
      '
        Name: $bar->baz() on line 6
        Expression `$bar->baz()` calls function `array::baz`.
        Function `array::baz` not found.
      ',
    ]);
  }

}
