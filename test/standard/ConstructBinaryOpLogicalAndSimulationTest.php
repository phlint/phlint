<?php

use \phlint\Test as PhlintTest;

class ConstructBinaryOpLogicalAndSimulationTest {

  /**
   * Test in combination with various types.
   *
   * @test @internal
   */
  static function variousTypesTest () {
    PhlintTest::assertIssues('
      dump(1 and 1);
      dump("a" and 2);
      dump(null and false);
    ', [
      '
        Dump: dump(1 and 1) on line 1
        Argument `1 and 1` evaluates to `bool(true)`.
      ',
      '
        Dump: dump("a" and 2) on line 2
        Argument `"a" and 2` evaluates to `bool(true)`.
      ',
      '
        Dump: dump(null and false) on line 3
        Argument `null and false` evaluates to `bool(false)`.
      ',
    ]);
  }

}
