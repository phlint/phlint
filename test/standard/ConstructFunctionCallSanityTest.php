<?php

use \phlint\Test as PhlintTest;

class ConstructFunctionCallSanityTest {

  /**
   * Test that comparing excluding guarantees against
   * function parameters does not cause any issues.
   *
   * @test @internal
   */
  static function excludingGuaranteesCall () {
    PhlintTest::assertNoIssues('
        function foo ($bar) {
          if ($bar)
            baz($bar);
        }
        function baz (int $fun) {}
    ');
  }

}
