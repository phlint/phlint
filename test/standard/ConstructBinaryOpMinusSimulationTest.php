<?php

use \phlint\Test as PhlintTest;

class ConstructBinaryOpMinusSimulationTest {

  /**
   * Test in combination with various types.
   *
   * @test @internal
   */
  static function variousTypesTest () {
    PhlintTest::assertIssues('
      dump(1 - 1);
      dump("a" - 2);
      dump(null - false);
    ', [
      '
        Dump: dump(1 - 1) on line 1
        Argument `1 - 1` evaluates to `int(0)`.
      ',
      '
        Dump: dump("a" - 2) on line 2
        Argument `"a" - 2` evaluates to `int(-2)`.
      ',
      '
        Operand Compatibility: "a" on line 2
        Value `"a"` is always or sometimes of type `string`.
        Expression `"a" - 2` may cause undesired or unexpected behavior with `string` operands.
      ',
      '
        Dump: dump(null - false) on line 3
        Argument `null - false` evaluates to `int(0)`.
      ',
      '
        Operand Compatibility: null on line 3
        Value `null` is always or sometimes of type `null`.
        Expression `null - false` may cause undesired or unexpected behavior with `null` operands.
      ',
      '
        Operand Compatibility: false on line 3
        Value `false` is always or sometimes of type `bool`.
        Expression `null - false` may cause undesired or unexpected behavior with `bool` operands.
      ',
    ]);
  }

}
