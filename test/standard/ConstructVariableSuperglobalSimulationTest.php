<?php

use \phlint\Test as PhlintTest;

class ConstructVariableSuperglobalSimulationTest {

  /**
   * Test specialization with a superglobal.
   *
   * @test @internal
   */
  static function superglobalTemplateSpecialization () {
    PhlintTest::assertIssues('
      function foo ($bar) {
        return $bar . "!";
      }
      foo($_GET);
    ', [
      '
        Operand Compatibility: $bar on line 2
        Variable `$bar` is always or sometimes of type `mixed[string]`.
        Expression `$bar . "!"` may cause undesired or unexpected behavior with `mixed[string]` operands.
          Trace #1:
            #1: Function *function foo (mixed[string] $bar)* specialized for the expression *foo($_GET)* on line 4.
      ',
    ]);
  }

}
