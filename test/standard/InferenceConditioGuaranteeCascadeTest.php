<?php

use \phlint\Test as PhlintTest;

class InferenceConditioGuaranteeCascadeTest {

  /**
   * Test `is_array` in a deterministic type conversion inside a condition guarantee.
   *
   * @test @internal
   */
  static function deterministicTypeConversion () {
    PhlintTest::assertIssues('
      $foo = "";
      if (null !== $foo) {
        if (!is_array($foo))
          $foo = [$foo . "!"];
        $foo->bar();
      }
    ', [
      '
        Name: $foo->bar() on line 5
        Expression `$foo->bar()` calls function `string[]::bar`.
        Function `string[]::bar` not found.
      ',
    ]);
  }

}
