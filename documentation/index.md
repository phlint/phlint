Phlint - PHP Code Analyzer
==========================

Introduction
------------

Phlint is a tool with an aim to help maintain quality of php code by analyzing code and pointing out potential code
issues. It focuses on how the code works rather than how the code looks. Phlint is designed from the start to do
deep semantic analysis rather than doing only shallow or stylistic analysis.

Documentation
-------------

1. [Getting Started](getting-started.md)
2. [Configuration](configuration.md)
3. [Rules](rules.md)
4. [Glossary](glossary/index.md)
5. [Internal Design](internal-design.md)
