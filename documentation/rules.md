2. Rules
========

Rules define what is a potentially problematic code. Each rule looks for a specific pattern and warns the programmer
if any code matches that pattern. Some rules are such that they, for example, warn about code which causes a fatal
error and everyone agrees that following them is a good practice. However some rules are more subtle and they
are subject to opinions and different coding styles. For this reason it is encouraged that the rules in Phlint should
be as specific as reasonably possible so that programmers have enough flexibility in picking the ones they want to
enforce.

Internally supported rules
--------------------------

- [Argument Compatibility](rule/argumentCompatibility.md)
- [Assert Construct](rule/assertConstruct.md)
- [Case Sensitive Naming](rule/caseSensitiveNaming.md)
- [Constraint Attribute](rule/constraintAttribute.md)
- [Declaration Type](rule/declarationType.md)
- [Duplicate Array Declaration Key](rule/duplicateArrayDeclarationKey.md)
- [Exit Construct](rule/exitConstruct.md)
- [Import Usage](rule/importUsage.md)
- [Isolated Attribute](rule/isolatedAttribute.md)
- [Name](rule/name.md)
- [Operand Compatibility](rule/operandCompatibility.md)
- [PHPDoc](rule/phpDoc.md)
- [Pure Attribute](rule/pureAttribute.md)
- [Redeclaring](rule/redeclaring.md)
- [Short Open Tag](rule/shortOpenTag.md)
- [Source Syntax](rule/sourceSyntax.md)
- [Variable Append Initialization](rule/variableAppendInitialization.md)
- [Variable Initialization](rule/variableInitialization.md)
- [Variable Variable](rule/variableVariable.md)
