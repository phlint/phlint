<?php

namespace phlint\printer;

use \luka8088\phops as op;
use \phlint\data;
use \phlint\IIData;
use \phlint\inference;
use \phlint\node as pnode;
use \PhpParser\Node;

class Dump extends \PhpParser\PrettyPrinter\Standard {

  static function create () {
    return new self();
  }

  function printNode ($node) {
    return $this->handleMagicTokens($this->p($node));
  }

  function pExpr_Array (Node\Expr\Array_ $node) {
    if ($node->getAttribute('kind', Node\Expr\Array_::KIND_SHORT) === Node\Expr\Array_::KIND_LONG)
      return 'array(' . $this->pCommaSeparated($node->items) . ')';
    return '[' . $this->pCommaSeparated($node->items) . ']';
  }

  function pExpr_ConstFetch (Node\Expr\ConstFetch $node) {
    if (in_array(strtolower($node->name->toString()), ['false', 'true']))
      return 'bool(' . $this->p($node->name) . ')';
    return $this->p($node->name);
  }

  function pScalar_DNumber( Node\Scalar\DNumber $node) {
    return 'float(' . parent::pScalar_DNumber($node) . ')';
  }

  function pScalar_LNumber (Node\Scalar\LNumber $node) {
    return 'int(' . parent::pScalar_LNumber($node) . ')';
  }

  function pScalar_String (Node\Scalar\String_ $node) {
    return 'string(' . parent::pScalar_String($node) . ')';
  }

  function pYield ($node) {
    return implode('|', array_map(function ($yieldNode) {
      // @todo: Rethink.
      if ($yieldNode instanceof pnode\SymbolAlias)
        return '';
      if ($yieldNode instanceof data\Value && !$yieldNode->name && count($yieldNode->constraints) == 1)
        return inference\Symbol::phpID(end($yieldNode->constraints));
      if ($yieldNode instanceof data\Value)
        return $yieldNode->name;
      return $this->p($yieldNode);
    }, $node->yield));
  }

}
