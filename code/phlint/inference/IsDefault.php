<?php

namespace phlint\inference;

class IsDefault {

  function getIdentifier () {
    return 'isDefault';
  }

  function getDependencies () {
    return [
      'nodeRelation',
    ];
  }

  static function set ($node, $value) {
    $node->iiData['isDefault'] = $value;
  }

  static function get ($node) {
    return isset($node->iiData['isDefault']) && $node->iiData['isDefault'];
  }

}
