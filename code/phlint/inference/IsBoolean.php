<?php

namespace phlint\inference;

use \phlint\node as pnode;
use \PhpParser\Node;

class IsBoolean {

  function getIdentifier () {
    return 'isBoolean';
  }

  static function get ($node) {
    if ($node instanceof Node\Expr\ConstFetch && strtolower($node->name->toString()) == 'true')
      return true;
    if ($node instanceof Node\Expr\ConstFetch && strtolower($node->name->toString()) == 'false')
      return true;
    if ($node instanceof pnode\SymbolAlias && $node->id == 't_bool')
      return true;
    return false;

  }

}
