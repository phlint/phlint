<?php

namespace phlint\inference;

use \luka8088\phops as op;
use \phlint\data;
use \phlint\IIData;
use \phlint\inference;
use \phlint\node as pnode;
use \phlint\NodeConcept;
use \PhpParser\Node;

/**
 * @see https://en.wikipedia.org/wiki/Intersection_(set_theory)
 */
class Intersection {

  static function get ($left, $right) {

    if (is_array($left)) {
      $intersection = [];
      foreach ($left as $leftNode)
        foreach (inference\Intersection::get($leftNode, $right) as $intersectionNode)
          $intersection[] = $intersectionNode;
      return inference\UniqueNode::get($intersection);
    }

    if (is_array($right))
      return inference\Intersection::get($right, $left);

    if ($left instanceof data\Value) {
      $constraintsIntersaction = [$right];
      foreach ($left->constraints as $constraint)
        $constraintsIntersaction = inference\Intersection::get($constraint, $constraintsIntersaction);
      $z = [];
      $valueNodes = [];
      foreach ($constraintsIntersaction as $x)
        foreach ($x instanceof data\Value ? $x->constraints : [$x] as $y) {
          if ($y instanceof Node\Expr\Array_) {
            $valueNodes[] = $y;
            continue;
          }
          if (inference\Value::isValueNode($y)) {
            $valueNodes[] = $y;
            continue;
          }
          $z[] = $y;
        }
      if ($z === $left->constraints)
        return array_merge($valueNodes, [$left]);
      if ($right instanceof data\Value && $z === $right->constraints)
        return array_merge($valueNodes, [$right]);
      return array_merge($valueNodes, count($z) > 0 ? [new data\Value(inference\UniqueNode::get($z))] : []);
    }

    if ($right instanceof data\Value)
      return inference\Intersection::get($right, $left);

    if ($left instanceof pnode\Excludes) {
      if (inference\IsMixed::get($left->node) && $right instanceof pnode\Excludes && inference\IsMixed::get($right->node))
        return [$left];
      if (inference\IsMixed::get($left->node))
        return [];
      if (inference\IsMixed::get($right))
        return [$left];
      // @todo: Fix
      if ($right instanceof pnode\Excludes) {
        $constraints = inference\UniqueNode::get(data\Value::extractConstraints([$left->node, $right->node]));
        return count($constraints) <= 1 ? $constraints : [new pnode\Excludes(new data\Value($constraints))];
      }
      $intersection = inference\Intersection::get($right, $left->node instanceof data\Value ? $left->node->constraints : $left->node);
      if (count($intersection) == 0)
        return is_array($right) ? $right : [$right];
      if ($intersection === [$left->node]) {
        // @todo: Fix
        $intersection = inference\Intersection::get($left->node, $right);
        return $intersection === [$left->node] ? [$right] : [$left];
      }
      return [];
    }

    if ($right instanceof pnode\Excludes)
      return inference\Intersection::get($right, $left);

    if ($left instanceof Node\Expr\Array_ && $right instanceof Node\Expr\Array_)
      return $left->items === $right->items ? [$left] : [];

    if ($left instanceof Node\Expr\Array_ && inference\IsArray::get($right))
      return [$left];

    if (inference\IsArray::get($left) && $right instanceof Node\Expr\Array_)
      return [$right];

    if ($left instanceof pnode\SymbolAlias && inference\Symbol::isArray($left->id) && inference\IsArray::get($right))
      return [$left];

    if (inference\IsArray::get($left) && $right instanceof pnode\SymbolAlias && inference\Symbol::isArray($right->id))
      return [$right];

    if ($left instanceof Node\Expr\ConstFetch && $right instanceof Node\Expr\ConstFetch)
      if (strtolower($left->name->toString()) == strtolower($right->name->toString()))
        if (in_array($left->name->toString(), ['true', 'false', 'null']))
          return [$left];

    if ($left instanceof Node\Scalar\DNumber && $right instanceof Node\Scalar\DNumber)
      return $left->value == $right->value ? [$left] : [];

    if ($left instanceof Node\Scalar\DNumber && inference\IsFloat::get($right))
      return [$left];

    if (inference\IsFloat::get($left) && $right instanceof Node\Scalar\DNumber)
      return [$right];

    if ($left instanceof Node\Scalar\LNumber && $right instanceof Node\Scalar\LNumber)
      return $left->value == $right->value ? [$left] : [];

    if ($left instanceof Node\Scalar\LNumber && inference\IsInteger::get($right))
      return [$left];

    if (inference\IsInteger::get($left) && $right instanceof Node\Scalar\LNumber)
      return [$right];

    if ($left instanceof Node\Scalar\String_ && $right instanceof Node\Scalar\String_)
      return $left->value == $right->value ? [$left] : [];

    if ($left instanceof Node\Scalar\String_ && inference\IsString::get($right))
      return [$left];

    if (inference\IsString::get($left) && $right instanceof Node\Scalar\String_)
      return [$right];

    if ($left instanceof pnode\SymbolAlias && $left->id == 't_dynamic')
      return [$right];

    if ($right instanceof pnode\SymbolAlias && $right->id == 't_dynamic')
      return [$left];

    if ($left instanceof pnode\SymbolAlias && in_array(substr($left->id, 0, 2), ['c_', 'n_'])
        && $right instanceof pnode\SymbolAlias && in_array(substr($right->id, 0, 2), ['c_', 'n_'])) {
      if (inference\IsImplicitlyConvertible::get($right, $left))
        return [$left];
      if (inference\IsImplicitlyConvertible::get($left, $right))
        return [$right];
      return $left->id == $right->id ? [$left] : [];
    }

    if ($left instanceof pnode\SymbolAlias && in_array(substr($left->id, 0, 2), ['c_', 'n_'])
        && (inference\IsObject::get($right) || $right instanceof pnode\SymbolAlias && $right->id == 'o_object'))
      return [$left];

    if ((inference\IsObject::get($left) || $left instanceof pnode\SymbolAlias && $left->id == 'o_object')
        && $right instanceof pnode\SymbolAlias && in_array(substr($right->id, 0, 2), ['c_', 'n_']))
      return [$right];

    if (inference\IsMixed::get($left))
      return [$right];

    if (inference\IsMixed::get($right))
      return [$left];

    if (inference\IsNull::get($left) && inference\IsNull::get($right))
      return [$left];

    if ($left instanceof pnode\SymbolAlias && $right instanceof pnode\SymbolAlias && $left->id == $right->id)
      return [$left];

    return [];

  }

}
