<?php

namespace phlint\inference;

use \phlint\node as pnode;
use \PhpParser\Node;

class IsMixed {

  function getIdentifier () {
    return 'isMixed';
  }

  static function get ($node) {
    if ($node instanceof pnode\SymbolAlias && $node->id == 't_mixed')
      return true;
    return false;
  }

}
