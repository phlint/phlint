<?php

namespace phlint\inference;

use \phlint\data;
use \phlint\node as pnode;
use \PhpParser\Node;

class IsNull {

  function getIdentifier () {
    return 'isNull';
  }

  static function get ($node) {
    if ($node instanceof data\Value)
      foreach ($node->constraints as $constraint)
        if (self::get($constraint))
          return true;
    if ($node instanceof Node\Expr\ConstFetch && strtolower($node->name->toString()) == 'null')
      return true;
    if ($node instanceof pnode\SymbolAlias && $node->id == 'o_null')
      return true;
    return false;
  }

}
