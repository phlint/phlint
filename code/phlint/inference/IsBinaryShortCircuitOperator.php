<?php

namespace phlint\inference;

use \PhpParser\Node;

/**
 * All binary operators that support short-circuit evaluation.
 *
 * @see https://en.wikipedia.org/wiki/Short-circuit_evaluation
 */
class IsBinaryShortCircuitOperator {

  function getIdentifier () {
    return 'isBinaryShortCircuitOperator';
  }

  static function get ($node) {
    if ($node instanceof Node\Expr\BinaryOp\BooleanAnd)
      return true;
    if ($node instanceof Node\Expr\BinaryOp\BooleanOr)
      return true;
    if ($node instanceof Node\Expr\BinaryOp\Coalesce)
      return true;
    if ($node instanceof Node\Expr\BinaryOp\LogicalAnd)
      return true;
    if ($node instanceof Node\Expr\BinaryOp\LogicalOr)
      return true;
    return false;
  }

}
