<?php

namespace phlint\inference;

use \phlint\data;
use \phlint\node as pnode;
use \PhpParser\Node;

class IsFalse {

  function getIdentifier () {
    return 'isFalse';
  }

  static function get ($node) {
    if ($node instanceof Node\Expr\ConstFetch && strtolower($node->name->toString()) == 'false')
      return true;
    return false;
  }

}
