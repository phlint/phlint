<?php

namespace phlint\inference;

use \luka8088\phops as op;
use \phlint\data;
use \phlint\IIData;
use \phlint\inference;
use \phlint\node as pnode;
use \phlint\NodeConcept;
use \PhpParser\Node;

class BinaryOperationEvaluation {

  function getIdentifier () {
    return 'binaryOperationEvaluation';
  }

  static $operatorMap = [
    'PhpParser\Node\Expr\AssignOp\BitwiseAnd' => '&',
    'PhpParser\Node\Expr\AssignOp\BitwiseOr' => '|',
    'PhpParser\Node\Expr\AssignOp\BitwiseXor' => '^',
    'PhpParser\Node\Expr\AssignOp\Concat' => '.',
    'PhpParser\Node\Expr\AssignOp\Div' => '/',
    'PhpParser\Node\Expr\AssignOp\Minus' => '-',
    'PhpParser\Node\Expr\AssignOp\Mod' => '%',
    'PhpParser\Node\Expr\AssignOp\Mul' => '*',
    'PhpParser\Node\Expr\AssignOp\Plus' => '+',
    'PhpParser\Node\Expr\AssignOp\Pow' => '**',
    'PhpParser\Node\Expr\AssignOp\ShiftLeft' => '<<',
    'PhpParser\Node\Expr\AssignOp\ShiftRight' => '>>',
    'PhpParser\Node\Expr\BinaryOp\BitwiseAnd' => '&',
    'PhpParser\Node\Expr\BinaryOp\BitwiseOr' => '|',
    'PhpParser\Node\Expr\BinaryOp\BitwiseXor' => '^',
    'PhpParser\Node\Expr\BinaryOp\BooleanAnd' => '&&',
    'PhpParser\Node\Expr\BinaryOp\BooleanOr' => '||',
    'PhpParser\Node\Expr\BinaryOp\Coalesce' => '??',
    'PhpParser\Node\Expr\BinaryOp\Concat' => '.',
    'PhpParser\Node\Expr\BinaryOp\Div' => '/',
    'PhpParser\Node\Expr\BinaryOp\Equal' => '==',
    'PhpParser\Node\Expr\BinaryOp\Greater' => '>',
    'PhpParser\Node\Expr\BinaryOp\GreaterOrEqual' => '>=',
    'PhpParser\Node\Expr\BinaryOp\Identical' => '===',
    'PhpParser\Node\Expr\BinaryOp\LogicalAnd' => 'and',
    'PhpParser\Node\Expr\BinaryOp\LogicalOr' => 'or',
    'PhpParser\Node\Expr\BinaryOp\LogicalXor' => 'xor',
    'PhpParser\Node\Expr\BinaryOp\Minus' => '-',
    'PhpParser\Node\Expr\BinaryOp\Mod' => '%',
    'PhpParser\Node\Expr\BinaryOp\Mul' => '*',
    'PhpParser\Node\Expr\BinaryOp\NotEqual' => '!=',
    'PhpParser\Node\Expr\BinaryOp\NotIdentical' => '!==',
    'PhpParser\Node\Expr\BinaryOp\Plus' => '+',
    'PhpParser\Node\Expr\BinaryOp\Pow' => '**',
    'PhpParser\Node\Expr\BinaryOp\ShiftLeft' => '<<',
    'PhpParser\Node\Expr\BinaryOp\ShiftRight' => '>>',
    'PhpParser\Node\Expr\BinaryOp\Smaller' => '<',
    'PhpParser\Node\Expr\BinaryOp\SmallerOrEqual' => '<=',
    'PhpParser\Node\Expr\BinaryOp\Spaceship' => '<=>',
  ];

  static function get ($operator, $left, $right) {

    if (is_array($left)) {
      $evaluations = [];
      foreach ($left as $leftNode)
        foreach (inference\BinaryOperationEvaluation::get($operator, $leftNode, $right) as $evaluationNode)
          $evaluations[] = $evaluationNode;
      if (count($evaluations) == 0 && in_array($operator, ['&&', '||']))
        return [new data\Value([new pnode\SymbolAlias('t_bool')])];
      return inference\UniqueNode::get($evaluations);
    }

    if (is_array($right)) {
      $evaluations = [];
      foreach ($right as $rightNode)
        foreach (inference\BinaryOperationEvaluation::get($operator, $left, $rightNode) as $evaluationNode)
          $evaluations[] = $evaluationNode;
      if (count($evaluations) == 0 && in_array($operator, ['&&', '||']))
        return [new data\Value([new pnode\SymbolAlias('t_bool')])];
      return inference\UniqueNode::get($evaluations);
    }

    if ($left instanceof data\Value) {
      $evaluations = [];
      $evaluationsValues = [];
      foreach ($left->constraints as $constraint)
        foreach (inference\BinaryOperationEvaluation::get($operator, $constraint, $right) as $sumNode) {
          if ($sumNode instanceof Node\Expr\Array_) {
            $evaluationsValues[] = $sumNode;
            continue;
          }
          if (inference\Value::isValueNode($sumNode)) {
            $evaluationsValues[] = $sumNode;
            continue;
          }
          $evaluations[] = $sumNode;
        }
      return array_merge($evaluationsValues, count($evaluations) == 0 ? [] : [new data\Value(inference\UniqueNode::get($evaluations))]);
    }

    if ($right instanceof data\Value)
      return inference\BinaryOperationEvaluation::get($operator, $right, $left);

    if ($left instanceof Node\Expr\Array_ && inference\IsArray::get($right))
      return [$right];

    if (inference\IsArray::get($left) && $right instanceof Node\Expr\Array_)
      return [$left];

    if (inference\IsArray::get($left) && inference\IsNull::get($right)) {
      if (in_array($operator, ['===']))
        return [new Node\Expr\ConstFetch(new Node\Name('false'))];
      return [];
    }

    if (inference\IsArray::get($left) xor inference\IsArray::get($right))
      return [];

    if ($left instanceof Node\Scalar\DNumber && $right instanceof Node\Scalar\DNumber)
      return [
        new Node\Scalar\DNumber(inference\BinaryOperationEvaluation::value($operator, $left->value, $right->value))
      ];

    if ($left instanceof Node\Scalar\DNumber && $right instanceof Node\Scalar\LNumber)
      return [
        new Node\Scalar\DNumber(inference\BinaryOperationEvaluation::value($operator, $left->value, $right->value))
      ];

    if ($left instanceof Node\Scalar\LNumber && $right instanceof Node\Scalar\DNumber)
      return [
        new Node\Scalar\DNumber(inference\BinaryOperationEvaluation::value($operator, $left->value, $right->value))
      ];

    if ($left instanceof Node\Scalar\LNumber && $right instanceof Node\Scalar\LNumber) {
      if ($operator == '/' && $right->value == 0)
        return [new Node\Scalar\DNumber(\INF)];
      return [
        inference\Value::scalarNode(inference\BinaryOperationEvaluation::value($operator, $left->value, $right->value))
      ];
    }

    if ($left instanceof Node\Scalar\LNumber && $right instanceof Node\Scalar\String_)
      return [
        inference\Value::scalarNode(inference\BinaryOperationEvaluation::value($operator, $left->value, $right->value))
      ];

    if ($left instanceof Node\Scalar\LNumber && inference\IsFalse::get($right))
      return [inference\Value::scalarNode(inference\BinaryOperationEvaluation::value($operator, $left->value, false))];

    if ($left instanceof Node\Scalar\LNumber && inference\IsMixed::get($right)) {
      $yield = [];
      if (in_array($operator, ['*', '**', '+', '-', '/']))
        $yield[] = new data\Value([new pnode\SymbolAlias('t_float')]);
      if (in_array($operator, ['%', '&', '*', '**', '+', '-', '/', '<<', '>>', '^', '|']))
        $yield[] = new data\Value([new pnode\SymbolAlias('t_int')]);
      return $yield;
    }

    if ($left instanceof Node\Scalar\String_ && $right instanceof Node\Scalar\LNumber) {
      if ($operator == '??')
        return [$left];
      return [
        inference\Value::scalarNode(inference\BinaryOperationEvaluation::value($operator, $left->value, $right->value))
      ];
    }

    if ($left instanceof Node\Scalar\String_ && $right instanceof Node\Scalar\String_)
      return [
        inference\Value::scalarNode(inference\BinaryOperationEvaluation::value($operator, $left->value, $right->value))
      ];

    if ($left instanceof Node\Scalar\String_ && inference\IsMixed::get($right)) {
      $yield = [];
      if (in_array($operator, ['*', '**', '+', '-', '/']))
        $yield[] = new data\Value([new pnode\SymbolAlias('t_float')]);
      if (in_array($operator, ['%', '&', '*', '**', '+', '-', '/', '<<', '>>', '^', '|']))
        $yield[] = new data\Value([new pnode\SymbolAlias('t_int')]);
      if (in_array($operator, ['.']))
        $yield[] = new data\Value([new pnode\SymbolAlias('t_string')]);
      return $yield;
    }

    if (inference\IsBoolean::get($left) && inference\IsNull::get($right)) {
      if (in_array($operator, ['===']))
        return [new Node\Expr\ConstFetch(new Node\Name('false'))];
      return [];
    }

    if (inference\IsFalse::get($left) && $right instanceof Node\Scalar\LNumber)
      return [inference\Value::scalarNode(inference\BinaryOperationEvaluation::value($operator, false, $right->value))];

    if (inference\IsFalse::get($left) && inference\IsFalse::get($right))
      return [inference\Value::scalarNode(inference\BinaryOperationEvaluation::value($operator, false, false))];

    if (inference\IsFalse::get($left))
      return inference\BinaryOperationEvaluation::get($operator, $operator == '.' ? new Node\Scalar\String_('') : new Node\Scalar\LNumber(0), $right);

    if (inference\IsTrue::get($left) && $right instanceof Node\Scalar\LNumber)
      return [inference\Value::scalarNode(inference\BinaryOperationEvaluation::value($operator, true, $right->value))];

    if (inference\IsTrue::get($left) && inference\IsTrue::get($right))
      return [inference\Value::scalarNode(inference\BinaryOperationEvaluation::value($operator, true, true))];

    if (inference\IsTrue::get($left)) {
      if ($operator == '||')
        return [$left];
      return inference\BinaryOperationEvaluation::get($operator, new Node\Scalar\LNumber(1), $right);
    }

    if (inference\IsTrue::get($right))
      return inference\BinaryOperationEvaluation::get($operator, $right, $left);

    if (inference\IsNull::get($left) && $right instanceof Node\Scalar\LNumber)
      return [inference\Value::scalarNode(inference\BinaryOperationEvaluation::value($operator, null, $right->value))];

    if (inference\IsNull::get($left) && inference\IsFalse::get($right))
      return [inference\Value::scalarNode(inference\BinaryOperationEvaluation::value($operator, null, false))];

    if (inference\IsNull::get($left) && inference\IsBoolean::get($right)) {
      if (in_array($operator, ['===']))
        return [new Node\Expr\ConstFetch(new Node\Name('false'))];
      return [];
    }

    if (inference\IsNull::get($left)) {
      if ($operator == '??')
        return [$right];
      return inference\BinaryOperationEvaluation::get($operator, $operator == '.' ? new Node\Scalar\String_('') : new Node\Scalar\LNumber(0), $right);
    }

    if (inference\IsNull::get($right))
      return inference\BinaryOperationEvaluation::get($operator, $left, $operator == '.' ? new Node\Scalar\String_('') : new Node\Scalar\LNumber(0));

    if (inference\IsObject::get($left))
      return inference\BinaryOperationEvaluation::get($operator, new Node\Scalar\LNumber(1), $right);

    if (inference\IsMixed::get($left) && !inference\IsMixed::get($right))
      return inference\BinaryOperationEvaluation::get($operator, $right, $left);

    if (inference\IsFalse::get($right))
      return inference\BinaryOperationEvaluation::get($operator, $left, $operator == '.' ? new Node\Scalar\String_('') : new Node\Scalar\LNumber(0));

    if (inference\IsObject::get($right))
      return inference\BinaryOperationEvaluation::get($operator, $left, new Node\Scalar\LNumber(1));

    if ($operator == '.')
      return [
        new data\Value([new pnode\SymbolAlias('t_string')]),
      ];

    // @todo: Return empty by default.
    return [
      new data\Value([new pnode\SymbolAlias('t_int')]),
    ];

  }

  /** @internal */
  static function value ($operator, $left, $right) {
    switch ($operator) {
      case '%':
        return $right == 0 ? \NAN : ((is_numeric($left) ? $left : 0) % (is_numeric($right) ? $right : 0));
      case '&&':
        return $left && $right;
      case '&':
        return (is_numeric($left) ? $left : 0) & (is_numeric($right) ? $right : 0);
      case '*':
        return (is_numeric($left) ? $left : 0) * (is_numeric($right) ? $right : 0);
      case '**':
        return pow(is_numeric($left) ? $left : 0, is_numeric($right) ? $right : 0);
      case '+':
        return (is_numeric($left) ? $left : 0) + (is_numeric($right) ? $right : 0);
      case '-':
        return (is_numeric($left) ? $left : 0) - (is_numeric($right) ? $right : 0);
      case '.':
        return $left . $right;
      case '/':
        return $right == 0 ? \INF : ((is_numeric($left) ? $left : 0) / $right);
      case '<':
        return $left < $right;
      case '<<':
        return (is_numeric($left) ? $left : 0) << (is_numeric($right) ? $right : 0);
      case '<=':
        return $left <= $right;
      case '<=>':
        return ($left < $right) ? -1 : (($left > $right) ? 1 : 0);
      case '==':
        return $left == $right;
      case '===':
        return $left === $right;
      case '>':
        return $left > $right;
      case '>=':
        return $left >= $right;
      case '>>':
        return (is_numeric($left) ? $left : 0) >> (is_numeric($right) ? $right : 0);
      case '??':
        return $left !== null ? $left : $right;
      case '^':
        return (is_numeric($left) ? $left : 0) ^ (is_numeric($right) ? $right : 0);
      case 'and':
        return $left and $right;
      case 'or':
        return $left or $right;
      case 'xor':
        return $left xor $right;
      case '|':
        return (is_numeric($left) ? $left : 0) | (is_numeric($right) ? $right : 0);
      case '||':
        return $left || $right;
      default:
        assert(false);
    }
  }

}
