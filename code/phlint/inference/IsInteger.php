<?php

namespace phlint\inference;

use \phlint\node as pnode;
use \PhpParser\Node;

class IsInteger {

  function getIdentifier () {
    return 'isInteger';
  }

  static function get ($node) {
    if ($node instanceof Node\Scalar\LNumber)
      return true;
    if ($node instanceof pnode\SymbolAlias && $node->id == 't_int')
      return true;
    return false;
  }

}
