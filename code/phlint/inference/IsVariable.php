<?php

namespace phlint\inference;

use \phlint\node as pnode;
use \PhpParser\Node;

class IsVariable {

  function getIdentifier () {
    return 'isVariable';
  }

  static function get ($node) {

    if ($node instanceof Node\Expr\Variable)
      return true;

    $scopeClass = class_exists(Node\Identifier::class)
      ? pnode\Scope::class
      : pnode\ScopeV3::class;

    if ($node instanceof $scopeClass)
      return self::get($node->expression);

    return false;

  }

}
