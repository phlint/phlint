<?php

namespace phlint\inference;

use \phlint\node as pnode;
use \PhpParser\Node;

class IsObject {

  function getIdentifier () {
    return 'isObject';
  }

  static function get ($node) {
    if ($node instanceof pnode\SymbolAlias && $node->id == 'o_object')
      return true;
    if ($node instanceof pnode\SymbolAlias && $node->id == 't_object')
      return true;
    if ($node instanceof pnode\SymbolAlias && in_array(substr($node->id, 0, 2), ['c_', 'n_']))
      return true;
    return false;
  }

}
