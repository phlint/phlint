<?php

namespace phlint\inference;

use \phlint\inference;
use \phlint\node as pnode;
use \PhpParser\Node;

class IsArray {

  function getIdentifier () {
    return 'isArray';
  }

  static function get ($node) {
    if ($node instanceof Node\Expr\Array_)
      return true;
    if ($node instanceof pnode\SymbolAlias && $node->id == 't_array')
      return true;
    if ($node instanceof pnode\SymbolAlias && inference\Symbol::isArray($node->id))
      return true;
    return false;
  }

}
