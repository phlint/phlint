<?php

namespace phlint\inference;

use \phlint\data;
use \phlint\node as pnode;
use \PhpParser\Node;

class IsTrue {

  function getIdentifier () {
    return 'isTrue';
  }

  static function get ($node) {
    if ($node instanceof Node\Expr\ConstFetch && strtolower($node->name->toString()) == 'true')
      return true;
    return false;
  }

}
