<?php

namespace phlint\inference;

use \phlint\node as pnode;
use \PhpParser\Node;

class IsString {

  function getIdentifier () {
    return 'isString';
  }

  static function get ($node) {
    if ($node instanceof Node\Scalar\String_)
      return true;
    if ($node instanceof pnode\SymbolAlias && $node->id == 't_string')
      return true;
    return false;
  }

}
