<?php

namespace phlint\inference;

use \phlint\node as pnode;
use \PhpParser\Node;

class IsFloat {

  function getIdentifier () {
    return 'isFloat';
  }

  static function get ($node) {
    if ($node instanceof Node\Scalar\DNumber)
      return true;
    if ($node instanceof pnode\SymbolAlias && $node->id == 't_float')
      return true;
    return false;
  }

}
