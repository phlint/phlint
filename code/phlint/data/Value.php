<?php

namespace phlint\data;

class Value {

  public $constraints = [];
  public $name = '';

  function __construct ($constraints, $name = '') {
    $this->constraints = self::extractConstraints($constraints);
    $this->name = $name;
  }

  static function extractConstraints ($node) {

    if (is_array($node)) {
      $constraints = [];
      foreach ($node as $subNode)
        foreach (self::extractConstraints($subNode) as $constraint)
          $constraints[] = $constraint;
      return $constraints;
    }

    if ($node instanceof self)
      return $node->constraints;

    return [$node];

  }

}
