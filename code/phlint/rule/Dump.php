<?php

namespace phlint\rule;

use \luka8088\phops\MetaContext;
use \phlint\inference;
use \phlint\MarkdownBuilder;
use \phlint\node as pnode;
use \phlint\NodeConcept;
use \phlint\printer\Dump as DumpPrinter;
use \phlint\Result;
use \PhpParser\Node;

/**
 * The purpose of this rule is to be used for testing and debugging purposes.
 *
 * This rule prints out all calls to `dump` function with the arguments
 * evaluated.
 *
 * For example:
 *
 *   dump(1 + 1);
 *
 * Will yield:
 *
 *   Argument `1 + 1` evaluates to `int(2)`.
 *
 */
class Dump {

  function getIdentifier () {
    return 'dump';
  }

  function getCategories () {
    return [
      'debug',
    ];
  }

  function getInferences () {
    return [
      'callArgument',
      'evaluation',
      'expressionSpecialization',
      'isReachable',
      'nodeRelation',
      'symbolLink',
    ];
  }


  function visitNode ($node) {

    if (!inference\IsReachable::get($node))
      return;

    if (!NodeConcept::isInvocationNode($node))
      return;

    $yieldClass = class_exists(Node\Identifier::class)
      ? pnode\Yield_::class
      : pnode\YieldV3::class;

    $callArguments = inference\CallArgument::get(inference\NodeRelation::originNode($node));

    foreach (inference\ExpressionSpecialization::get($node) as $specializedNode)
      if (inference\SymbolLink::getUnmangled($specializedNode) == ['f_dump'])
        foreach ($callArguments as $index => $callArgument)
          MetaContext::get(Result::class)->addViolation(
            $node,
            $this->getIdentifier(),
            'Dump',
            ucfirst(NodeConcept::referencePrint($node->args[$index])) . ' evaluates to '
              . MarkdownBuilder::inlineCode(DumpPrinter::create()->printNode(
                new $yieldClass(inference\Evaluation::get($callArgument))
              )) . '.',
            ''
          );

  }

}
